﻿using System;
using Calculator.Interfaces;
using Calculator.Resource;

namespace Calculator.Models
{
    public class StringToNumberWorker: IConverterOfTypes<string, double>
    {    
        public double ConvertType(string inputString)
        {
            bool isValidNumber = false;
            bool isFirst = true;
            double result = default;

            while (!isValidNumber)
            {
                try
                {
                    if (!isFirst)
                    {
                        Console.WriteLine(StringContent.ReinputNumber);
                        inputString = Console.ReadLine();
                    }

                    isFirst = false;
                    result = double.Parse(inputString);
                    isValidNumber = true;
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return result;
        }
    }
}
