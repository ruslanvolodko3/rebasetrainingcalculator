﻿using System;
using Domain.Calculator.Enums;
using Domain.Calculator.Models;

namespace Calculator.Models.OperationResolvers
{
    public class StringOperationsOnNumbersResolver : IOperationsOnNumbersResolver<string>
    {
        public OperationsOnNumbers GetOperation(string inputParametr)
        {
            switch (inputParametr)
            {
                case "+":
                    return OperationsOnNumbers.Addition;

                case "-":
                    return OperationsOnNumbers.Addition;

                case "/":
                    return OperationsOnNumbers.Addition;

                case "*":
                    return OperationsOnNumbers.Addition;

                default: throw new ArgumentException();
            }
        }
    }
}
