﻿using System;
using Domain.Calculator.Enums;
using Domain.Calculator.Models;

namespace Calculator.Models.OperationResolvers
{
    class CharOperationsOnNumbersResolver : IOperationsOnNumbersResolver<char>
    {
        public OperationsOnNumbers GetOperation(char inputParametr)
        {
            switch (inputParametr)
            {
                case '+':
                    return OperationsOnNumbers.Addition;

                case '-':
                    return OperationsOnNumbers.Addition;

                case '/':
                    return OperationsOnNumbers.Addition;

                case '*':
                    return OperationsOnNumbers.Addition;

                default: throw new ArgumentException();
            }
        }
    }
}
