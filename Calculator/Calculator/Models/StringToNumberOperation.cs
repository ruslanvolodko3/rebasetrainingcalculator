﻿using System;
using Calculator.Interfaces;
using Calculator.Models.OperationResolvers;
using Calculator.Resource;
using Domain.Calculator.Enums;
using Domain.Calculator.Models;

namespace Calculator.Models
{
    class StringToNumberOperation : IConverterOfTypes<string, OperationsOnNumbers>
    {
        public OperationsOnNumbers ConvertType(string inputString)
        {
            IOperationsOnNumbersResolver<string> operationsOnNumbersResolver = new StringOperationsOnNumbersResolver();
            bool isValidOperation = false;
            OperationsOnNumbers result = OperationsOnNumbers.Addition;
            bool isFirst = true;

            while (!isValidOperation)
            {
                try
                {
                    if (!isFirst)
                    {
                        Console.WriteLine(StringContent.InputOperation);
                        inputString = Console.ReadLine();
                    }

                    isFirst = false;
                    result = operationsOnNumbersResolver.GetOperation(inputString);
                    isValidOperation = true;
                }
                catch (ArgumentException e)
                {
                    Console.WriteLine(e.Message);
                }
            }

            return result;
        }
    }
}
