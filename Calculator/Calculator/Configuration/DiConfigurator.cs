﻿using Microsoft.Extensions.DependencyInjection;
using Calculator.Interfaces;
using Calculator.Models;
using Domain.Calculator.Enums;
using Domain.Calculator.Interfaces;
using Domain.Calculator.Models;

namespace Calculator.Configuration
{
    public static class DiConfigurator
    {
        public static ServiceProvider ConfigureDi()
        {
            var serviceProvider = new ServiceCollection()
                .AddLogging()
                .AddSingleton<ICalculatorWorker, CalculatorWorker>()
                .AddSingleton<IConverterOfTypes<string, double>, StringToNumberWorker>()
                .AddSingleton<IConverterOfTypes<string, OperationsOnNumbers>, StringToNumberOperation>()
                .BuildServiceProvider();

            return serviceProvider;
        }
    }
}
