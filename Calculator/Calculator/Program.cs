﻿using System;
using Microsoft.Extensions.DependencyInjection;
using Calculator.Configuration;
using Calculator.Interfaces;
using Calculator.Resource;
using Domain.Calculator.Enums;
using Domain.Calculator.Interfaces;

namespace Calculator
{
    class Program
    {
        static void Main()
        {
            string exitCommand = string.Empty;
            double firstOperand, secondOperand;
            ServiceProvider serviceProvider = DiConfigurator.ConfigureDi();
            OperationsOnNumbers operationsOnNumbers;
            ICalculatorWorker calculatorWorker = serviceProvider.GetService<ICalculatorWorker>();
            IConverterOfTypes<string, double> stringToNumberWorker = serviceProvider.GetService<IConverterOfTypes<string, double>>();
            IConverterOfTypes<string, OperationsOnNumbers> stringToNumberOperation = serviceProvider.GetService<IConverterOfTypes<string, OperationsOnNumbers>>();

            while (exitCommand != StringContent.ExitCommnd)
            {
                Console.WriteLine(StringContent.InputFirstNumber);
                firstOperand = stringToNumberWorker.ConvertType(Console.ReadLine());
                Console.WriteLine(StringContent.InputSecondNumber);
                secondOperand = stringToNumberWorker.ConvertType(Console.ReadLine());
                Console.WriteLine(StringContent.InputOperation);
                operationsOnNumbers = stringToNumberOperation.ConvertType(Console.ReadLine());
                Console.WriteLine(StringContent.GetResult(calculatorWorker.GetResult(operationsOnNumbers, firstOperand, secondOperand)));
                Console.WriteLine(StringContent.FinalPhrase);
                exitCommand = Console.ReadLine();
            }
        }
    }
}
