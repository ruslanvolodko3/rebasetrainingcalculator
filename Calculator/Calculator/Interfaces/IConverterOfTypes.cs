﻿namespace Calculator.Interfaces
{
    public interface IConverterOfTypes<Tin, Tout>
    {
        Tout ConvertType(Tin inputString);
    }
}
