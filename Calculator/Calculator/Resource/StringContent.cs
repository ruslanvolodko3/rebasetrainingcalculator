﻿namespace Calculator.Resource
{
    public static class StringContent
    {
        public static string ExitCommnd => "q";

        public static string InputFirstNumber => "Input first number:";

        public static string InputSecondNumber => "Input second number:";

        public static string ReinputNumber => "Reinput number please:";

        public static string InputOperation => "Input operation:";

        public static string FinalPhrase => "Input \"q\" if your want to exit or another key tu continue.";

        public static string GetResult(double result) => $"Resilt is {result}";

    }
}
