﻿using Domain.Calculator.Enums;

namespace Domain.Calculator.Interfaces
{
    public interface ICalculatorWorker
    {
        double GetResult(OperationsOnNumbers operationsOnNumbers, double firstOperand, double secondOperand);
    }
}
