﻿namespace Domain.Calculator.Interfaces
{
    internal interface ICalculator
    {
        internal double AddNumbers(double firstOperand, double secondOperand);

        internal double Subtraction(double firstOperand, double secondOperand);

        internal double Division(double firstOperand, double secondOperand);

        internal double Multiply(double firstOperand, double secondOperand);
    }
}
