﻿using Domain.Calculator.Enums;

namespace Domain.Calculator.Models
{
    public interface IOperationsOnNumbersResolver<T>
    {
        public  OperationsOnNumbers GetOperation(T inputParametr);
    }
}
