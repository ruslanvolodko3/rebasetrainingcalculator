﻿using Domain.Calculator.Interfaces;

namespace Domain.Calculator.Models
{
    internal class Calculator : ICalculator
    {
        double ICalculator.AddNumbers(double firstOperand, double secondOperand)
        {
            return firstOperand + secondOperand;
        }

        double ICalculator.Subtraction(double firstOperand, double secondOperand)
        {
            return firstOperand - secondOperand;
        }

        double ICalculator.Division(double firstOperand, double secondOperand)
        {
            return firstOperand / secondOperand;
        }

        public double Multiply(double firstOperand, double secondOperand)
        {
            return firstOperand * secondOperand;
        }
    }
}
