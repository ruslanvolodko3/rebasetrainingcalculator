﻿using System;
using Domain.Calculator.Enums;
using Domain.Calculator.Interfaces;

namespace Domain.Calculator.Models
{
    public class CalculatorWorker : ICalculatorWorker
    {
        private readonly ICalculator _calculator;

        public CalculatorWorker()
        {
            _calculator = new Calculator();
        }

        public double GetResult(OperationsOnNumbers operationsOnNumbers, double firstOperand, double secondOperand)
        {
            switch (operationsOnNumbers)
            {
                case OperationsOnNumbers.Addition:
                    return _calculator.AddNumbers(firstOperand, secondOperand);

                case OperationsOnNumbers.Subtraction:
                    return _calculator.Subtraction(firstOperand, secondOperand);

                case OperationsOnNumbers.Division:
                    return _calculator.Division(firstOperand, secondOperand);

                case OperationsOnNumbers.Multiplication:
                    return _calculator.Multiply(firstOperand, secondOperand);

                default: 
                    throw new ArgumentException();
            }
        }
    }
}