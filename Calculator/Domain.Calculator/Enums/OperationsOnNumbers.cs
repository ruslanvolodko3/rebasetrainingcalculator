﻿namespace Domain.Calculator.Enums
{
    public enum OperationsOnNumbers
    {
        Addition,
        Subtraction,
        Division,
        Multiplication
    }
}
